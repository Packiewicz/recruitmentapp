# Description

Project made for educational purposes. It is system intedent to aid recruitment process. Logged in users can create recruitments (title, job description, salary etc.) and also can apply for them.

Project's backend is made with C# ASP.MVC 5 and as ORM it uses MongoDB. It needs to have supplied connection string, because i dont want to share password to mine DB :). Frontend was realized using React.JS.

After interactions with recruitments, this application, sends emails to users (e.g informing about succesful application). For it to work its needed to create file *email.config* with some necessary information. Example below:

```
<appSettings>
  <add key="smtpClient" value="smtp.email.com"/>
  <add key="emailAddress" value="example@email.com"/>
  <add key="emailPassword" value="supersecret"/>
  <add key="emailPort" value="666"/>
  <add key="connectionString" value="my_mongo_database"/>
</appSettings>
```