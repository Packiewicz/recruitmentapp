﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace RecuitmentApp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                name: "AddRecruitment",
                url: "recruitments/new",
                defaults: new {controller = "FormCreator", action = "AddForm" }
                );
            //sciezka do pobrania rekrutacji z bazy
            routes.MapRoute(
                name: "Recruitments",
                url: "recruitments",
                defaults: new { controller = "Recruitments", action = "Recruitments" }
                );
            //sciezka do pobrania wszystkich aplikacji
            routes.MapRoute(
                name: "Applications",
                url: "applications",
                defaults: new { controller = "RecruiterPanel", action = "Applications" }
                );
            //sciezka do pobrania konkretnej aplikacji
            routes.MapRoute(
                name: "Application",
                url: "application",
                defaults: new { controller = "RecruiterPanel", action = "JsonApplication" }
                );

            routes.MapRoute(
                name: "EditEmails",
                url:"Admin/Edit",
                defaults: new { controller = "Admin", action = "Edit", id = ""}
                );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Recruitments", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
