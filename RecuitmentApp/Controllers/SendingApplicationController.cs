﻿using MongoDB.Bson;
using MongoDB.Driver;
using RecuitmentApp.Models;
using RecuitmentApp.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace RecuitmentApp.Controllers
{
    public class SendingApplicationController : Controller
    {
        private MongoDAO daoObject = new MongoDAO();
        // GET: SendingApplication
        public ActionResult Index(string whichRecruitment) // build form to display
        {
            DynamicFormModel dfm = new DynamicFormModel();

            ObjectId id = new ObjectId(whichRecruitment);
            var filter = Builders<RecruitmentModel>.Filter.Eq(r => r.Id, id);
            var recruitment = daoObject.GetRecruitment(filter);

            dfm.formTitle = recruitment.formDocument.Tytul;
            dfm.RecruitmentId = recruitment.Id;

            StringWriter stringWriter = new StringWriter();
            HtmlTextWriter html = new HtmlTextWriter(stringWriter);

            var controlsList = recruitment.formDocument.PolaFormy;
            
            using (html)
            {
                if (controlsList != null && controlsList.Any())
                {
                    foreach (PoleFormy pole in controlsList)
                    {
                        switch(pole.Typ.ToLower())
                        {
                            case "text":
                                {
                                    html = RenderTextTag(html, pole);
                                    break;
                                }
                            case "select":
                                {
                                    html = RenderSelectTag(html, pole);
                                    break;
                                }
                            case "radio":
                                {
                                    html = RenderRadioTag(html, pole);
                                    break;
                                }
                            case "checkbox":
                                {
                                    html = RenderCheckboxTag(html, pole);
                                    break;
                                }
                        }

                    }
                }
            }

            dfm.formHtml = stringWriter.ToString();

            return View(dfm);
        }

        [HttpPost]
        public ActionResult Index(FormCollection fc)   // get results from form
        {
            List<string> skipKeys = new List<string> { "__RequestVerificationToken", "Email", "Imie", "Nazwisko", "NrTelefonu", "RecruitmentId" };

            ObjectId id = new ObjectId(fc["RecruitmentId"]);    // get recruitment that is submitted
            var filter = Builders<RecruitmentModel>.Filter.Eq(r => r.Id, id);

            RecruitmentModel recruitment = daoObject.GetRecruitment(filter);
            var fields = recruitment.formDocument.PolaFormy;

            PoleFormy tempField;

            ApplicationModel applicationModel = new ApplicationModel();

            List<FormAnswer> application = new List<FormAnswer>();
            foreach (string key in fc.AllKeys)
            {
                if (skipKeys.Contains(key))
                    continue;

                tempField = fields.Find(r => r.Podpis == key);  // find field that name is the same as FormCollection key
                FormAnswer fa = new FormAnswer();   // create form answer object
                fa.Podpis = tempField.Podpis;
                fa.CzyWielokrotnyWybor = tempField.CzyWielokrotnyWybor;
                fa.Typ = tempField.Typ;

                string[] stringArr;
                if (tempField.Typ != "Text")
                {
                    stringArr = fc[key].Split(',');    //  check if single answer

                    if (stringArr.Count() > 1)
                    {
                        fa.ListaOdpowiedzi = stringArr.ToList();         // multiple answers

                        application.Add(fa);
                    }
                    else if (stringArr.Count() == 1)
                    {
                        fa.Odpowiedz = fc[key];         // single answer

                        application.Add(fa);
                    }
                }
                else
                {
                    fa.Odpowiedz = fc[key];         // single answer

                    application.Add(fa);
                }
                
            }

            foreach(string key in skipKeys)
            {
                if (key == "__RequestVerificationToken")
                    continue;

                applicationModel.GetType().GetProperty(key).SetValue(applicationModel, fc[key]);
            }
            applicationModel.FormAnswers = application;

            daoObject.InsertDocumentToCollection(applicationModel, MongoDAO.APPLICATIONS_COLLECTION);

            SendingEmail se = new SendingEmail();
            se.SendEmail(applicationModel.Email, SendingEmail._ACK_APPLICATION_SUBJECT);
            
            return RedirectToAction("Index", "Recruitments");
        }

        private HtmlTextWriterTag GetInputTypeTagNumber(string type)
        {
            switch(type)
            {
                case "select":
                {
                        return HtmlTextWriterTag.Option;
                }
                case "checkbox":
                case "radio":
                {
                        return HtmlTextWriterTag.Label;
                }
                default:
                    {
                        return HtmlTextWriterTag.Option;
                    }
            }

        }

        private HtmlTextWriter RenderTextTag(HtmlTextWriter writer, PoleFormy pole)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "group");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            writer.AddAttribute("required", "required");
            writer.AddAttribute(HtmlTextWriterAttribute.Name, pole.Podpis);
            writer.RenderBeginTag(HtmlTextWriterTag.Textarea);

            writer.RenderEndTag();    // end input tag

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "highlight");
            writer.RenderBeginTag(HtmlTextWriterTag.Span);
            writer.RenderEndTag();

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "bar");
            writer.RenderBeginTag(HtmlTextWriterTag.Span);
            writer.RenderEndTag();

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "textLabel");
            writer.RenderBeginTag(HtmlTextWriterTag.Label);
            writer.Write(pole.Podpis);
            writer.RenderEndTag();

            writer.RenderEndTag();       // end div tag

            return writer;
        }

        private HtmlTextWriter RenderSelectTag(HtmlTextWriter writer, PoleFormy pole)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "select");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            writer.RenderBeginTag(HtmlTextWriterTag.Label);
            writer.Write(pole.Podpis);
            writer.RenderEndTag();

            writer.AddAttribute("required", "required");
            if (pole.CzyWielokrotnyWybor)
            {
                writer.AddAttribute("multiple", "multiple");
                writer.AddAttribute("size", "1");
            }
            writer.AddAttribute(HtmlTextWriterAttribute.Name, pole.Podpis);
            writer.RenderBeginTag(HtmlTextWriterTag.Select);

            if (pole.Opcje != null && pole.Opcje.Any())   // jeśli ma cokolwiek
            {
                foreach (String opcja in pole.Opcje)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Value, opcja);    //value of option
                    writer.RenderBeginTag(HtmlTextWriterTag.Option); // option
                    writer.Write(opcja);
                    writer.RenderEndTag();    // end option
                }
            }

            writer.RenderEndTag();    // end input tag

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "select__arrow");    // render arrow in select
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.RenderEndTag();

            writer.RenderEndTag();       // end div tag

            return writer;
        }

        private HtmlTextWriter RenderRadioTag(HtmlTextWriter writer, PoleFormy pole)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "group");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            //label ogolny
            writer.RenderBeginTag(HtmlTextWriterTag.Label);
            writer.Write(pole.Podpis);
            writer.RenderEndTag();

            if (pole.Opcje != null && pole.Opcje.Any())   // jeśli ma cokolwiek
            {
                foreach (String opcja in pole.Opcje)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "control control--radio");
                    writer.RenderBeginTag(HtmlTextWriterTag.Label);       // open label
                    writer.Write(opcja);

                    writer.AddAttribute(HtmlTextWriterAttribute.Type, "radio");    // input begin
                    writer.AddAttribute(HtmlTextWriterAttribute.Value, opcja);    // input begin
                    writer.AddAttribute(HtmlTextWriterAttribute.Name, pole.Podpis);    // input begin
                    writer.RenderBeginTag(HtmlTextWriterTag.Input);
                    writer.RenderEndTag();    // end input

                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "control__indicator");
                    writer.RenderBeginTag(HtmlTextWriterTag.Div); // indicator
                    writer.RenderEndTag();       // end div

                    writer.RenderEndTag();    // end label
                }
            }

            writer.RenderEndTag();       // end div tag

            return writer;
        }

        private HtmlTextWriter RenderCheckboxTag(HtmlTextWriter writer, PoleFormy pole)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "group");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            if(pole.Opcje == null || !pole.Opcje.Any())
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "control control--checkbox");
            }
            writer.RenderBeginTag(HtmlTextWriterTag.Label);
            writer.Write(pole.Podpis);

            if (pole.Opcje != null && pole.Opcje.Any())   // jeśli wiele opcji
            {
                writer.RenderEndTag();      // end label

                foreach (String opcja in pole.Opcje)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "control control--checkbox");
                    writer.RenderBeginTag(HtmlTextWriterTag.Label);
                    writer.Write(opcja);

                    writer.AddAttribute(HtmlTextWriterAttribute.Type, "checkbox");    
                    writer.AddAttribute(HtmlTextWriterAttribute.Name, pole.Podpis);   
                    writer.AddAttribute(HtmlTextWriterAttribute.Value, opcja);    //value of option
                    writer.RenderBeginTag(HtmlTextWriterTag.Input); // option
                    writer.RenderEndTag();       // end input

                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "control__indicator");
                    writer.RenderBeginTag(HtmlTextWriterTag.Div); // indicator
                    writer.RenderEndTag();       // end div

                    writer.RenderEndTag();       // end label
                }
            }
            else       // pojedynczy
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Type, "checkbox");    //value of option
                writer.AddAttribute(HtmlTextWriterAttribute.Name, "checkbox" + pole.Podpis);
                writer.AddAttribute(HtmlTextWriterAttribute.Value, pole.Podpis);    //value of option
                writer.RenderBeginTag(HtmlTextWriterTag.Input); // option
                writer.RenderEndTag();       // end input

                writer.AddAttribute(HtmlTextWriterAttribute.Class, "control__indicator");
                writer.RenderBeginTag(HtmlTextWriterTag.Div); // indicator
                writer.RenderEndTag();       // end div

                writer.RenderEndTag();       // end label
            }

            writer.RenderEndTag();       // end div tag

            return writer;
        }

    }
}