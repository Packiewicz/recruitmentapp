﻿using MongoDB.Bson;
using MongoDB.Driver;
using RecuitmentApp.Models;
using RecuitmentApp.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RecuitmentApp.Controllers
{
    public class RecruiterPanelController : Controller
    {
        MongoDAO daoObject = new MongoDAO();
        // GET: RecruiterPanel
        [Authorize]
        public ActionResult IndexRecruitments()
        {
            return View();
        }

        [Authorize]
        public ActionResult EditRecruitment(string whichRecruitment)
        {
            ObjectId id = new ObjectId(whichRecruitment);
            var filter = Builders<RecruitmentModel>.Filter.Eq(r => r.Id, id);
            var recruitment = daoObject.GetRecruitment(filter);

            return View(recruitment);
        }

        [Authorize]
        [HttpPost]
        public ActionResult EditRecruitment(RecruitmentModel recruitment)
        {
            var filter = Builders<RecruitmentModel>.Filter.Eq(r => r.Id, recruitment.Id);
            RecruitmentModel rec = daoObject.GetRecruitment(filter);

            rec.IsOpen = recruitment.IsOpen;
            rec.EndDate = recruitment.EndDate;

            daoObject.UpdateRecruitment(rec);

            return View("IndexRecruitments");
        }

        [Authorize]
        public ActionResult Recruitment(string whichRecruitment)
        {
            ObjectId id = new ObjectId(whichRecruitment);
            var filter = Builders<RecruitmentModel>.Filter.Eq(r => r.Id, id);
            var recruitment = daoObject.GetRecruitment(filter);

            return View("Recruitment", recruitment);
        }

        [Authorize]
        public ActionResult CloseOpenRecruitment(string whichRecruitment, bool isOpen)
        {
            ObjectId id = new ObjectId(whichRecruitment);
            RecruitmentModel recruitment;

            var filter = Builders<RecruitmentModel>.Filter.Eq(r => r.Id, id);
            recruitment = daoObject.GetRecruitment(filter);
            recruitment.IsOpen = isOpen;
            daoObject.UpdateRecruitment(recruitment);

            return View("IndexRecruitments");
        }

        public ActionResult DeleteRecruitment(string whichRecruitment)
        {
            daoObject.DeleteRecruitment(whichRecruitment);

            return View("IndexRecruitments");
        }

        [Authorize]
        public ActionResult Applications(string whichRecruitment)
        {
            var filter = Builders<ApplicationModel>.Filter.Eq("RecruitmentId", whichRecruitment);

            List<ApplicationModel> applications = daoObject.GetApplications(filter);

            return Json(applications, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult ApplicationsIndex()
        {
            return View();
        }

        [Authorize]
        public ActionResult ApplicationDetails(string whichApplication)
        {
            ViewBag.whichApp = whichApplication;

            return View();
        }

        [Authorize]
        public ActionResult JsonApplication(string whichApplication)
        {
            ObjectId id = new ObjectId(whichApplication);
            var filter = Builders<ApplicationModel>.Filter.Eq(r => r.Id, id);

            var applications = daoObject.GetApplications(filter);

            return Json(applications.First(), JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult AcceptApplication(string whichApplication)
        {
            ObjectId id = new ObjectId(whichApplication);
            var filter = Builders<ApplicationModel>.Filter.Eq(r => r.Id, id);

            var application = daoObject.GetApplications(filter);

            SendingEmail se = new SendingEmail();

            se.SendEmail(application.First().Email, SendingEmail._ACCEPT_APPLICATION_SUBJECT);

            daoObject.AcceptDeclineApplication(whichApplication, true);

            return View("ApplicationsIndex");
        }

        [Authorize]
        public ActionResult DenyApplication(string whichApplication)
        {
            ObjectId id = new ObjectId(whichApplication);
            var filter = Builders<ApplicationModel>.Filter.Eq(r => r.Id, id);

            var application = daoObject.GetApplications(filter);

            SendingEmail se = new SendingEmail();

            se.SendEmail(application.First().Email, SendingEmail._REJECT_APPLICATION_SUBJECT);

            daoObject.AcceptDeclineApplication(whichApplication, false);

            return View("ApplicationsIndex");
        }
    }
}