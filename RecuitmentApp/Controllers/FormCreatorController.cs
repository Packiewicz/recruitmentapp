﻿using RecuitmentApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RecuitmentApp.Controllers
{
    public class FormCreatorController : Controller
    {
        private MongoDAO daoObject = new MongoDAO();

        // GET: FormCreator
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult AddForm(FormDocumentModel form, string date)
        {
            RecruitmentModel recruitment = new RecruitmentModel();
            recruitment.formDocument = form;
            recruitment.EndDate = date;

            daoObject.InsertDocumentToCollection(recruitment, MongoDAO.RECRUITMENTS_COLLECTION);
            return View("IndexRecruitments", "RecruiterPanel");
        }
    }
}