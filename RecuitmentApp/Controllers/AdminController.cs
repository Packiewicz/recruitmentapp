﻿using MongoDB.Bson;
using RecuitmentApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RecuitmentApp.Controllers
{
    public class AdminController : Controller
    {
        private MongoDAO daoObject = new MongoDAO();

        // GET: Admin
        public ActionResult Index()
        {
            var emails = daoObject.GetEmails(null);

            return View(emails);
        }

        //get
        public ActionResult Edit(string id)
        {
            var emails = daoObject.GetEmails(id);

            return View(emails);
        }

        [HttpPost]
        public ActionResult Edit(EmailsModel emails, string id)
        {
            ObjectId objectId = new ObjectId(id);
            emails.Id = objectId;

            daoObject.UpdateEmail(emails);

            return View("Index", emails);
        }

        // GET: Admin
        public ActionResult IndexUsers()
        {
            var users = daoObject.GetUsers();

            return View(users.FindAll(r => r.IsAccepted == false)); // tylko niezaakceptowani uzytkownicy
        }

        //get
        public ActionResult AcceptUser(string id)
        {
            daoObject.AcceptUser(id);

            var users = daoObject.GetUsers().FindAll(r => r.IsAccepted == false);


            return View("IndexUsers", users);
        }

        public ActionResult DeleteUser(string id)
        {
            daoObject.DeleteUser(id);

            var users = daoObject.GetUsers().FindAll(r => r.IsAccepted == false);

            return View("IndexUsers", users);
        }

    }
}
