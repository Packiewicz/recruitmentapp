﻿using RecuitmentApp.Models;
using RecuitmentApp.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace RecuitmentApp.Controllers
{
    public class LoginController : Controller
    {
        LoginMemberProvider provider = (LoginMemberProvider)Membership.Provider;

        private MongoDAO daoObject = new MongoDAO();
        // GET: Login
        public ActionResult LoginPage()
        {
            return View();
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoginPage(User user, string returnUrl)
        {
            if(!ValidateLogin(user))
            {
                return View(user);
            }
            // poprawne dane

            FormsAuthentication.SetAuthCookie(user.UserName, false);

            // obsluga return url
            if(!String.IsNullOrEmpty(returnUrl) && returnUrl != "/")
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        public ActionResult Register(User user)
        {
            User checkUser = daoObject.GetUser(user.UserName);

            if (checkUser == null) // jesli nie ma usera o takiej nazwie
            {
                string pwd = EncryptPassword(user.Password);    // encrypt password
                user.Password = pwd;

                //insert to db
                daoObject.InsertDocumentToCollection(user, MongoDAO.USER_COLLECTION);

                return View("LoginPage");
            }
            else
            {
                ModelState.AddModelError("username","Nazwa uzytkownika zajeta");
                return View();
            }
        }

        // sprawdz czy dane wpisane i czy poprawne
        private bool ValidateLogin(User user)
        {
            if (String.IsNullOrEmpty(user.UserName))
            {
                ModelState.AddModelError(String.Empty, "Podaj nazwe uzytkownika");
            }
            else if (String.IsNullOrEmpty(user.Password))
            {
                ModelState.AddModelError(String.Empty, "Podaj haslo");
            }
            //else if(user.UserName == "admin" && user.Password == "admin")
            //{
            //    return ModelState.IsValid;
            //}
            else if (!provider.ValidateUser(user.UserName, user.Password))
            {
                ModelState.AddModelError(String.Empty, "Niepoprawne dane logowania lub konto nie zostało aktywowane");
            }
            return ModelState.IsValid;
        }

        protected string EncryptPassword(string password)
        {
            byte[] pwdBytes = Encoding.GetEncoding("utf-8").GetBytes(password);
            byte[] hashBytes = System.Security.Cryptography.MD5.Create().ComputeHash(pwdBytes);
            return Encoding.GetEncoding("utf-8").GetString(hashBytes);
        }
    }
}