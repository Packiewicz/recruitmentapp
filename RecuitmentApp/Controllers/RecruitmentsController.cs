﻿using MongoDB.Bson;
using MongoDB.Driver;
using RecuitmentApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RecuitmentApp.Controllers
{
    public class RecruitmentsController : Controller
    {
        private MongoDAO daoObject = new MongoDAO();
        // GET: Recruitments
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Recruitment(string whichRecruitment)
        {
            ObjectId id = new ObjectId(whichRecruitment);
            var filter = Builders<RecruitmentModel>.Filter.Eq(r => r.Id, id);
            var recruitment = daoObject.GetRecruitment(filter);

            return View("Recruitment", recruitment);
        }

        public ActionResult Recruitments()  // funkcja do wywolania z js
        {
            List<RecruitmentModel> listaRekrutacji = daoObject.GetAllRecruitments();

            return Json(listaRekrutacji, JsonRequestBehavior.AllowGet);
        }
    }
}