﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RecuitmentApp.Models
{
    public class FormDocumentModel
    {
        [BsonElement("title")]
        public string Tytul { get; set; }

        [BsonElement("description")]
        public string Opis { get; set; }
        [BsonElement("requirements")]
        public string Wymagania { get; set; }
        [BsonElement("offer")]
        public string Oferujemy { get; set; }
        [BsonElement("price")]
        public string Zarobki { get; set; }

        [BsonElement("formFields")]
        public List<PoleFormy> PolaFormy { get; set; }

    }

    public class PoleFormy
    {
        [BsonElement("type")]
        public string Typ { get; set; }
        [BsonElement("label")]
        public string Podpis { get; set; }
        [BsonElement("isMultiChoice")]
        public bool CzyWielokrotnyWybor { get; set; }
        [BsonElement("selectOptions")]
        public List<string> Opcje { get; set; }
    }
}