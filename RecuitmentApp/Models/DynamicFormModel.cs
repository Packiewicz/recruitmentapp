﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;

namespace RecuitmentApp.Models
{
    public class DynamicFormModel
    {
        [BsonId]
        public ObjectId Id { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [Display(Name = "Imię")]
        public string Imie { get; set; }
        [Required]
        public string Nazwisko { get; set; }
        [Display(Name = "Numer Telefonu")]
        public string NrTelefonu { get; set; }

        public ObjectId RecruitmentId { get; set; }

        [BsonIgnore]
        public string formHtml { get; set; }
        [BsonIgnore]
        public string formTitle { get; set; }

        //public File CvFile;
    }
}