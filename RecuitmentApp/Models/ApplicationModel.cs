﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RecuitmentApp.Models
{
    public class ApplicationModel
    {
        [BsonId]
        public ObjectId Id { get; set; }

        public string RecruitmentId { get; set; }

        public string Imie { get; set; }
        public string Nazwisko { get; set; }
        public string Email { get; set; }
        public string NrTelefonu { get; set; }

        public List<FormAnswer> FormAnswers { get; set; }

        public bool IsAccepted { get; set; }
    }

    public class FormAnswer
    {
        [BsonElement("type")]
        public string Typ { get; set; }
        [BsonElement("label")]
        public string Podpis { get; set; }
        [BsonElement("isMultiChoice")]
        public bool CzyWielokrotnyWybor { get; set; }
        [BsonElement("answer")]
        public string Odpowiedz { get; set; }
        [BsonElement("answersList")]
        public List<string> ListaOdpowiedzi { get; set; }
    }
}