﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace RecuitmentApp.Models
{

    //http://www.hurryupandwait.io/blog/implementing-custom-membership-provider-and-role-provider-for-authenticating-asp-net-mvc-applications
    public class User : IPrincipal
    {
        [BsonId]
        public ObjectId UserID { get; set; }
        [BsonElement("username")]
        [Required]
        [Display(Name ="Nazwa użytkownika")]
        public string UserName { get; set; }
        [BsonElement("password")]
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Hasło")]
        public string Password { get; set; }
        [BsonElement("isAccepted")]
        [Display(Name = "Czy zaakceptowany")]
        public bool IsAccepted { get; set; } = false;

        public IIdentity Identity
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public bool IsInRole(string role)
        {
            throw new NotImplementedException();
        }
    }
}