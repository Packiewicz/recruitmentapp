﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RecuitmentApp.Models
{
    public class EmailsModel
    {
        [BsonId]
        public ObjectId Id { get; set; }
        [Display(Name="Powitanie")]
        public string Greeting { get; set; }
        [Display(Name = "Potwierdzenie zgłoszenia")]
        public string Confirm { get; set; }
        [Display(Name = "Akceptacja zgłoszenia")]
        public string Accept { get; set; }
        [Display(Name = "Odrzucenie zgłoszenia")]
        public string Deny { get; set; }
        [Display(Name = "Podpis")]
        public string Signature { get; set; }
    }
}