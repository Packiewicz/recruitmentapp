﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RecuitmentApp.Models
{
    public class RecruitmentModel
    {
        [BsonId]
        public ObjectId Id { get; set; }
        public FormDocumentModel formDocument { get; set; }
        public bool IsOpen { get; set; }
        public string EndDate { get; set; }
    }
}