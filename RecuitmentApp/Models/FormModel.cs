﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RecuitmentApp.Models
{
    public class FormModel
    {
        public string Title { get; set; }
        public List<FormElement> ElementsList { get; set; }
    }

    public class FormElement
    {
        public string Type { get; set; }    // type of input
        public string Description { get; set; }     // label of input
        public bool IsMultiSelect { get; set; }    
        public List<string> OptionsList { get; set; }
    }
}