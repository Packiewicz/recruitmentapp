﻿using MongoDB.Bson;
using MongoDB.Driver;
using RecuitmentApp.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace RecuitmentApp.Controllers
{
    public class MongoDAO
    {
        public MongoClient client { get; set; }
        public IMongoDatabase db { get; set; }

        private static string DB_NAME = "RecruitmentDB";

        public static string APPLICATIONS_COLLECTION = "Applications";
        public static string USER_COLLECTION = "Users";
        public static string RECRUITMENTS_COLLECTION = "Recruitments";
        public static string EMAILS_COLLECTION = "Emails";

        public static string USER_COLLECTION_USERNAME = "username";

        public static string FORM_COLLECTION_TITLE = "title";
        public static string RECRUITMENTS_COLLECTION_FORM = "formDocument";

        public MongoDAO()
        {
            client = new MongoClient(ConfigurationManager.AppSettings["connectionString"]);
            db = client.GetDatabase(DB_NAME);
        }

        public IMongoCollection<T> GetCollection<T>(string collectionName)
        {
            var collection = db.GetCollection<T>(collectionName);

            return collection;
        }

        public void InsertDocumentToCollection<T>(T document, string collectionName)
        {
            var collection = db.GetCollection<T>(collectionName);

            collection.InsertOne(document);
        }

        public User GetUser(string userName)
        {
            User user = null;

            var collection = GetCollection<User>(USER_COLLECTION);
            var filter = Builders<User>.Filter.Eq(USER_COLLECTION_USERNAME, userName);

            if (collection.Find(filter).Any())
            { 
                user = collection.Find(filter).First();   // ???
            }
            else
            {
                //do nothing
            }

            return user;
        }

        public List<User> GetUsers()
        {
            var collection = GetCollection<User>(USER_COLLECTION);
            var filter = Builders<User>.Filter.Empty;

            var result = collection.Find(filter);

            return result.ToList();
        }

        public void AcceptUser(string id)
        {
            ObjectId _id = new ObjectId(id);
            var filter = Builders<User>.Filter.Eq(r => r.UserID, _id);
            var update = Builders<User>.Update.Set(r => r.IsAccepted, true);

            var collection = GetCollection<User>(USER_COLLECTION);
            var result = collection.UpdateOne(filter, update);
        }

        public void DeleteUser(string id)
        {
            ObjectId _id = new ObjectId(id);
            var filter = Builders<User>.Filter.Eq(r => r.UserID, _id);

            var collection = GetCollection<User>(USER_COLLECTION);
            var result = collection.DeleteOne(filter);

            if (result != null)
            {
                Debug.WriteLine("cos");
            }
        }

        public void DeleteRecruitment(string id)
        {
            ObjectId _id = new ObjectId(id);
            var filter = Builders<RecruitmentModel>.Filter.Eq(r => r.Id, _id);

            var collection = GetCollection<RecruitmentModel>(RECRUITMENTS_COLLECTION);
            var result = collection.DeleteOne(filter);
        }

        public RecruitmentModel GetRecruitment(FilterDefinition<RecruitmentModel> filter)
        {
            RecruitmentModel recruitment = null;

            var collection = GetCollection<RecruitmentModel>(RECRUITMENTS_COLLECTION);
            try
            {
                recruitment = collection.Find(filter).First();
            }
            catch
            {
                //do nothing
            }
            return recruitment;
        }

        public List<RecruitmentModel> GetAllRecruitments()
        {
            var filter = Builders<RecruitmentModel>.Filter.Empty;

            var collection = GetCollection<RecruitmentModel>(RECRUITMENTS_COLLECTION);
            var result = collection.Find(filter);

            return result.ToList();
        }
        
        public List<ApplicationModel> GetApplications(FilterDefinition<ApplicationModel> filter)
        {
            var collection = GetCollection<ApplicationModel>(APPLICATIONS_COLLECTION);
            var result = collection.Find(filter);

            return result.ToList();
        }

        public void AcceptDeclineApplication(string id, bool isAccepted)
        {
            ObjectId _id = new ObjectId(id);
            var filter = Builders<ApplicationModel>.Filter.Eq(r => r.Id, _id);
            var update = Builders<ApplicationModel>.Update.Set(r => r.IsAccepted, isAccepted);

            var collection = GetCollection<ApplicationModel>(APPLICATIONS_COLLECTION);
            var result = collection.UpdateOne(filter, update);
        }

        public EmailsModel GetEmails(string id)
        {
            FilterDefinition<EmailsModel> filter;

            if (id == null)
            {
                filter = Builders<EmailsModel>.Filter.Empty;
            }
            else
            {
                ObjectId objectId = new ObjectId(id);
                filter = Builders<EmailsModel>.Filter.Eq(r => r.Id, objectId);
            }

            var collection = GetCollection<EmailsModel>(EMAILS_COLLECTION);
            var result = collection.Find(filter);

            return result.First();
        }

        public void UpdateEmail(EmailsModel emails)
        {
            var filter = Builders<EmailsModel>.Filter.Eq(r => r.Id, emails.Id);

            var collection = GetCollection<EmailsModel>(EMAILS_COLLECTION);
            var result = collection.FindOneAndReplace(filter, emails);

            if(result == null)
            {
                Debug.WriteLine(":(");
            }
        }

        public void UpdateRecruitment(RecruitmentModel recruitment)
        {
            var filter = Builders<RecruitmentModel>.Filter.Eq(r => r.Id, recruitment.Id);

            var collection = GetCollection<RecruitmentModel>(RECRUITMENTS_COLLECTION);
            var result = collection.FindOneAndReplace(filter, recruitment);
        }
    }
}