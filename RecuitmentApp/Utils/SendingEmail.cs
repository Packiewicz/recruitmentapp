﻿using RecuitmentApp.Controllers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace RecuitmentApp.Utils
{
    public class SendingEmail
    {
        public static string _ACK_APPLICATION_SUBJECT = "Potwierdzenie przyjęcia zgłoszenia";        // poprawnie przyjeto zgloszenie
        public static string _ACCEPT_APPLICATION_SUBJECT = "Przejście do kolejnego etapu";     // zaakceptowano do nastepnego etapu
        public static string _REJECT_APPLICATION_SUBJECT = "Odrzucenie zgłoszenia";     // odrzucono zgloszenie

        public void SendEmail(string receiverAddress, string subject)
        {
            MailMessage mail = new MailMessage();
            SmtpClient smtpClient = new SmtpClient(ConfigurationManager.AppSettings["smtpClient"]);

            mail.From = new MailAddress(ConfigurationManager.AppSettings["emailAddress"], "Aplikacja rekrutacji");
            mail.To.Add(receiverAddress);
            mail.Subject = subject;
            mail.Body = GetMessageBody(subject);

            smtpClient.Port = int.Parse(ConfigurationManager.AppSettings["emailPort"]);
            smtpClient.EnableSsl = true;
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpClient.UseDefaultCredentials = false;
            smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["emailAddress"],
                ConfigurationManager.AppSettings["emailPassword"]);

            smtpClient.Send(mail);
        }

        private string GetMessageBody(string subject)
        {
            MongoDAO daoObject = new MongoDAO();

            var emails = daoObject.GetEmails(null);

            string emailBody = emails.Greeting;

            if(subject == _ACK_APPLICATION_SUBJECT)
            {
                emailBody += "\n" + emails.Confirm;
            }
            else if(subject == _ACCEPT_APPLICATION_SUBJECT)
            {
                emailBody += "\n" + emails.Accept;
            }
            else if(subject == _REJECT_APPLICATION_SUBJECT)
            {
                emailBody += "\n" + emails.Deny;
            }

            emailBody += "\n" + emails.Signature;

            return emailBody;
        }
    }
}