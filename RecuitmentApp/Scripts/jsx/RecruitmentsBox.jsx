﻿const RecruitmentsBox = React.createClass({
    getInitialState()
    {
        return {
            data: [],
        };
    },

    componentWillMount()
    {
        var xhr = new XMLHttpRequest();
        xhr.open('get', this.props.url, true);
        xhr.onload = function () {
            var data = JSON.parse(xhr.responseText);
            this.setState({ data: data });
        }.bind(this);
        xhr.send();
    },

    render()
    {
        return (
            <div className="RecruitmentBox">
                <RecruitmentsList data={this.state.data}/>
            </div>
        );
    }
});

ReactDOM.render(
    <RecruitmentsBox url="/Recruitments"/>,
    document.getElementById('root')
    );