﻿var Modal = React.createClass({
    getInitialState(){
        return{
            inputType: null,
            inputIsMultiChoice: null,
            inputDesc: null,
        };
    },
    //http://freefrontend.com/css-input-text/
    handleTekstChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        if (target.type == 'select-one' && target.value == 'text')
        {
            $('#fieldset').attr('disabled', true);
            
            for (i = this.props.optionsList.length; i > 0 ; i--)    // wyczysc tablice
            {
                this.props.optionsList.pop();
            }

            this.forceUpdate();
        }
        else if (target.type == 'select-one')
        {
            $('#fieldset').removeAttr('disabled');
        }

        this.setState({
            [name]: value,
        });
    },

    handleAddOptionClick() {
        const domElementValue = document.getElementById('option').value;
        document.getElementById('option').value = '';   // wyczysc pole
        if (domElementValue != '') {
            this.props.optionsList.push(domElementValue);
            this.forceUpdate();
        }
    },

    render() {
        // Render nothing if the "show" prop is false
        if (this.props.show == false) {
            return null;
        }

        const htmlList = this.props.optionsList.map((element, index) => <li key={index}>{element}</li>);

        // The gray background
        const backdropStyle = {
            position: 'fixed',
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            backgroundColor: 'rgba(0,0,0,0.3)',
            padding: 50
        };

        // The modal "window"
        const modalStyle = {
            backgroundColor: '#fff',
            borderRadius: 5,
            maxWidth: 500,
            minHeight: 300,
            maxHeight: 700,
            margin: '0 auto',
            padding: 60
        };

        return (
            <div className="backdrop" style={backdropStyle}>
                <div className="myReactModal" style={modalStyle}>
                    <h1>Stwórz element formy</h1>

                    <form onSubmit={(event) => this.props.onSubmit(event)} method="post">
                            <label>Typ elementu</label>
                            <div className="select">
                                <select name="inputType" onChange={(event) => this.handleTekstChange(event) }>
                                        <option value="text">Text</option>
                                        <option value="radio">Radio</option>
                                        <option value="checkbox">CheckBox</option>
                                       <option value="select">Select List</option>
                                </select>
                                <div className="select__arrow"></div>
                            </div>
                        <br />
                        <div className="group">
                            <input id="desc" name="inputDesc" type="text" onChange={(event) => this.handleTekstChange(event) } required />
                            <span className="highlight"></span><span className="bar"></span>
                            <label className="textLabel">Opis</label>
                        </div>
                        <br />
                        <fieldset id="fieldset" disabled>
                            <div>
                                <label className="control control--checkbox">
                                    Wielokrotny wybór <input name="inputIsMultiChoice" type="checkbox" onChange={(event) => this.handleTekstChange(event) } />
                                    <div className="control__indicator"></div>
                                </label>
                            </div>
                            <br />
                            <div className="group">
                                <input id="option" name="inputOption" type="text" onChange={(event) => this.handleTekstChange(event) } />
                                <span className="highlight"></span><span className="bar"></span>
                                <label className="textLabel">Opcje</label>
                            </div>
                            <button className="btn btn-cancel" type="button" onClick={() => this.handleAddOptionClick() }>Dodaj opcje</button>
                        </fieldset>
                        <br />

                        <div>
                            <ul>
                                {htmlList}
                            </ul>
                        </div>

                        <div className="btn-box">
                            <input name="submit" type="submit" value="Submit" className="btn btn-submit"/>
                            <button className="btn btn-cancel" onClick={this.props.onClose}>
                                Close
                            </button>
                        </div>
                    </form>
                    </div >
                </div>
        );
    }
});
