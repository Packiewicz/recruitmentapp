﻿var Element = React.createClass(
    {
        render() {
            const elementStyle = {
                backgroundColor: 'rgba(0,0,0,0.1)',
                border: '2px solid',
                borderColor: '#cacaca',
            };

            return (
                <div className="formElement"  style={elementStyle}>
                    <p>{this.props.dataObject.Typ}</p>
                    <p>{this.props.dataObject.Podpis}</p>
                    <br />
                    <button className="btn btn-cancel" onClick={this.props.onDelete}>Delete</button>
                </div>
            );
        }
    });

