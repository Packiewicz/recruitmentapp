﻿function properString(str, len) {
    if (str.length < len) {
        for (i = 0; i < (len - str.length); i++) {
            str = '0' + str;
        }
    }

    return str;
}

const Box = React.createClass({
    render() {
        if (this.props.data.CzyWielokrotnyWybor) {
            const htmlList = this.props.data.ListaOdpowiedzi.map((element, index) => <li key={index}>{element}</li>);
            console.log(this.props.data)
            return (
                <div className={this.props.style}>
                    <h3>{this.props.data.Podpis}</h3>
                    <ul>{htmlList}</ul>
                </div>
            );
        }
        else {
            return (
                <div className={this.props.style}>
                    <h3>{this.props.data.Podpis}</h3>
                    {this.props.data.Odpowiedz}
                </div>
            );
        }
    }
});


const Application = React.createClass({
    getInitialState() {
        return {
            application: {},
            contactList: [],
        }
    },

    componentWillMount() {
        var url = '/application?whichApplication=';
        var root = document.getElementById('root');
        var param = root.getAttribute('data-param');
        url += param;

        console.log(url)

        var xhr = new XMLHttpRequest();
        xhr.open('get', url, true);
        xhr.onload = function () {
            var data = JSON.parse(xhr.responseText);

            console.log(data)

            var list = [];
            list.push('Imie: ' + data.Imie);
            list.push('Nazwisko: ' + data.Nazwisko);
            list.push('Nr telefonu: ' + data.NrTelefonu);
            list.push('Email: ' + data.Email);

            this.setState({ application: data, contactList: list });

        }.bind(this);
        xhr.send();
    },

    handleApplication(whichOne) {
        const obj = this.state.application.Id;

        var str1 = obj.Timestamp.toString(16);
        var str2 = obj.Machine.toString(16);
        var str3 = obj.Pid.toString(16);
        var str4 = obj.Increment.toString(16);

        str1 = properString(str1, 8);
        str2 = properString(str2, 6);
        str3 = properString(str3, 4);
        str4 = properString(str4, 6);

        const objID = str1 + str2 + str3 + str4;

        if (whichOne == 'accept')
        {
            var url = '/RecruiterPanel/AcceptApplication?whichApplication=';
        }
        else if (whichOne == 'decline')
        {
            var url = '/RecruiterPanel/DenyApplication?whichApplication=';
        }

        window.location.href = url + objID;
    },

    render() {

        const contactsStyle = {
            width: 300,
            height: 200,
            background: '#ECEFF1',
            padding: '1px 10px',
            margin: '5px',
            float: 'left'
        };

        const answersStyle = {
            width: '35%',
            margin: 5,
            background: '#ECEFF1',
            //padding: 1px 10px,
            float: 'left',
        };

        if (typeof this.state.application.FormAnswers === "undefined") {
            var elementsList = " ";
        }
        else
        {
            var elementsList = this.state.application.FormAnswers.map((element, index) => <Box key={index} data={element} style={answersStyle}></Box>);
            var contacts = this.state.contactList.map((element, index) => <li key={index}>{element}</li>);
        }

        return (
            <div id="main-stuff">
                <div className={contactsStyle}>
                    <h2>Dane kontaktowe</h2>
                    <ul id="contact-list">
                        {contacts}
                     </ul>
                </div>

                <div name="form-answers">
                    <ul>
                        {elementsList}
                    </ul>
                </div>
                <button className="btn btn-submit" type="button" onClick={() => this.handleApplication('accept')}>Zaakceptuj zgłoszenie</button>
                <button className="btn btn-submit" type="button" onClick={() => this.handleApplication('decline')}>Odrzuć zgłoszenie</button>
            </div>
            );
    }
});

ReactDOM.render(
    <Application />,
    document.getElementById('root')
);