﻿function properString(str, len)
{
    if (str.length < len)
    {
        for (i = 0; i < (len - str.length); i++)
        {
            str = '0' + str;
        }
    }

    return str;
}

function checkDate(endDate) {
    var now = new Date();

    var diff = endDate.getTime() - now.getTime();

    return diff / (1000 * 60 * 60 * 24);    // data zapisana jako ilosc milisekund od 1970
}

const RecruitmentElement = React.createClass({

    handleClickOnElement(event) {
        const obj = this.props.dataObject.Id;

        var str1 = obj.Timestamp.toString(16);
        var str2 = obj.Machine.toString(16);
        var str3 = obj.Pid.toString(16);
        var str4 = obj.Increment.toString(16);

        str1 = properString(str1, 8);
        str2 = properString(str2, 6);
        str3 = properString(str3, 4);
        str4 = properString(str4, 6);

        const objID = str1 + str2 + str3 + str4;

        const url = '/RecruiterPanel/recruitment?whichRecruitment=';

        window.location.href = url + objID;
    },

    render() {

        var locDate = new Date(this.props.dataObject.EndDate);

        var diff = checkDate(locDate);
        console.log(diff)

        if (this.props.dataObject.IsOpen == true && diff <= 7)  // otwarta i zbliza sie termin
        {
            classString = 'panel panel-danger';
        }
        else if (this.props.dataObject.IsOpen == true) {    // tylko otwarta
            classString = 'panel panel-primary';
        }
        else {      // zamknieta
            classString = 'panel panel-info';
        }

        return (
            <div id="recruitmentElement" className={classString} onClick={(event) => this.handleClickOnElement(event)} data-active={this.props.dataObject.IsOpen}>
                <div id="recruitmentTitle" className={classString}>
                    {this.props.dataObject.formDocument.Tytul}
                </div>
                <div className="panel-body">
                    {'Termin rekrutacji: '}
                    {this.props.dataObject.EndDate}
                </div>
            </div>
        );
    }
});

const RecruitmentsList = React.createClass({
    render() {
        var elementsList = this.props.data.map(function (element, index) {
            return (
                <RecruitmentElement key={index} dataObject={element} />
            );
        });

        return (
            <div className="recruitmentsList">
                {elementsList}
            </div>
        );
    }
});