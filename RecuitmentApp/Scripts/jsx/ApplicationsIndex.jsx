﻿function convertObjectIdToString (obj) {
    var str1 = obj.Timestamp.toString(16);
    var str2 = obj.Machine.toString(16);
    var str3 = obj.Pid.toString(16);
    var str4 = obj.Increment.toString(16);

    str1 = properString(str1, 8);
    str2 = properString(str2, 6);
    str3 = properString(str3, 4);
    str4 = properString(str4, 6);

    const objID = str1 + str2 + str3 + str4;
    return objID;
};

function properString(str, len) {
    if (str.length < len) {
        for (i = 0; i < (len - str.length); i++) {
            str = '0' + str;
        }
    }

    return str;
}

$(function () {
    $('select').empty();
    $('select').append($('<option selected="selected"></option>').val(0).html('Wybierz rekrutację'));
});

const ApplicationElement = React.createClass({

    handleClickOnElement(event) {
        const obj = this.props.dataObject.Id;

        var str1 = obj.Timestamp.toString(16);
        var str2 = obj.Machine.toString(16);
        var str3 = obj.Pid.toString(16);
        var str4 = obj.Increment.toString(16);

        str1 = properString(str1, 8);
        str2 = properString(str2, 6);
        str3 = properString(str3, 4);
        str4 = properString(str4, 6);

        const objID = str1 + str2 + str3 + str4;
        const url = '/RecruiterPanel/ApplicationDetails?whichApplication=';

        window.location.href = url + objID;
    },

    render() {
        if (this.props.dataObject.IsAccepted == false) {
            classString = 'panel panel-info';
        }
        else {
            classString = 'panel panel-primary';
        }
        return (
            <div id="recruitmentElement" className={classString} onClick={(event) => this.handleClickOnElement(event)}>
                <div className="panel-heading">
                    {this.props.dataObject.Imie}
                    {' '}
                    {this.props.dataObject.Nazwisko}
                </div>
            </div>
        );
    }
});

const ApplicationsApp = React.createClass({
    getInitialState() {
        return {
            recrutations: [],
            applications: [],
        }
    },

    componentWillMount() {
        var xhr = new XMLHttpRequest();
        xhr.open('get', '/Recruitments', true);
        xhr.onload = function () {
            var data = JSON.parse(xhr.responseText);

            this.setState({ recrutations: data });

            this.state.recrutations.map(function (element) {
                var idString = convertObjectIdToString(element.Id);
                $('select').append($('<option></option>').val(idString).html(element.formDocument.Tytul));
            });

        }.bind(this);
        xhr.send();
    },

    handleRecrutationChange(event) {
        const target = event.target;
        const value = target.value;

        const url = '/Applications?whichRecruitment=' + value;

        console.log(url)

        var xhr = new XMLHttpRequest();
        xhr.open('get', url, true);
        xhr.onload = function () {
            var data = JSON.parse(xhr.responseText);
            this.setState({ applications: data });
            console.log(this.state);
        }.bind(this);
        xhr.send();
    },

    render() {        

        var selectStyle = {
            width:'250px',
        };

        var applicationsList = this.state.applications.map(function (element, index) {
            return (
                <ApplicationElement key={index} dataObject={element} />
            );
        });

        return (
            <div>
                <div className="select">
                    <select style={selectStyle} name="recrutation" onChange={(event) => this.handleRecrutationChange(event)}>
                    
                    </select>
                    <div className="select__arrow"></div>
                </div>
                <div id="applicationsList">
                    {applicationsList}
                </div>
            </div>
            );
    }
});

ReactDOM.render(
    <ApplicationsApp />,
    document.getElementById('root')
);