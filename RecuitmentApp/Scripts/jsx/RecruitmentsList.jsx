﻿function properString(str, len) {
    if (str.length < len) {
        for (i = 0; i < (len - str.length); i++) {
            str = '0' + str;
        }
    }

    return str;
}

const RecruitmentElement = React.createClass({

    handleClickOnElement(event) {
        const obj = this.props.dataObject.Id;

        var str1 = obj.Timestamp.toString(16);
        var str2 = obj.Machine.toString(16);
        var str3 = obj.Pid.toString(16);
        var str4 = obj.Increment.toString(16);

        str1 = properString(str1, 8);
        str2 = properString(str2, 6);
        str3 = properString(str3, 4);
        str4 = properString(str4, 6);

        const objID = str1 + str2 + str3 + str4;
        const url = '/Recruitments/recruitment?whichRecruitment=';

        window.location.href = url + objID;
    },

    render() {
        console.log(this.props)

        if (this.props.dataObject.IsOpen == false)
        {
            return null;
        }
        return(
            <div id="recruitmentElement" className="panel panel-primary col-md-5" onClick={(event) => this.handleClickOnElement(event)}>
                <div className="panel-heading">
                    {this.props.dataObject.formDocument.Tytul}
                </div>
                <div className="panel-body">
                    {'Termin rekrutacji: '}
                    {this.props.dataObject.EndDate}
                </div>
            </div>
        );
    }
});

const RecruitmentsList = React.createClass({
    render() {
        var elementsList = this.props.data.map(function(element, index) {
            return(
               <RecruitmentElement key={index} dataObject={element } />
            );
        });

        return(
            <div id="recruitmentsList" className="recruitmentsList">
                {elementsList}
            </div>
        );
    }
});