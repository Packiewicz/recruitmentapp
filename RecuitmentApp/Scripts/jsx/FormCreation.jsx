﻿var FormCollection = React.createClass({
    getInitialState() {
        return {
            isModalOpen: 0,
            inputOptions: [],

            elementsList: [],

            formTitle: '',
            formDesc: '',
            formDemand: '',
            formOffer: '',
            formPrice: '',
            formDate: '',
        };
    },

    handleText(event) {
        const value = event.target.value;
        const name = event.target.name;

        this.setState({
            [name]: value,
        });
    },

    handleModal() { // open/close modal
        const ele = this.state.elementsList;
        const temp = this.state.isModalOpen == true ? false : true;
        this.setState({
            elementsList: ele,
            isModalOpen: temp,
            inputOptions: [],
        });
    },

    handleModalSubmit(event) {
        var options = this.state.inputOptions.slice(0);
        const elementObj = {
            Typ: event.target.inputType.value,
            Podpis: event.target.inputDesc.value,
            CzyWielokrotnyWybor: event.target.inputIsMultiChoice.checked,
            Opcje: options
        };

        const list = this.state.elementsList;
        list.push(elementObj);

        this.setState({
            elementsList: list,
            inputOptions: [],
        });

        this.handleModal();
        event.preventDefault();
    },

    hanldeFormCollectionSubmit() {
        const dataObj = {};
        dataObj['Tytul'] = this.state.formTitle;
        dataObj['Opis'] = this.state.formDesc;
        dataObj['Wymagania'] = this.state.formDemand;
        dataObj['Oferujemy'] = this.state.formOffer;
        dataObj['Zarobki'] = this.state.formPrice;
        dataObj['PolaFormy'] = this.state.elementsList;

        const data = {};
        data['form'] = dataObj;
        data['date'] = this.state.formDate;

        const strngf = JSON.stringify(data);

        var xhr = new XMLHttpRequest();
        xhr.open('post', this.props.submitUrl, true);
        xhr.setRequestHeader('Content-Type', 'application/json')
        xhr.onload = function () {
            location.href = '/RecruiterPanel/IndexRecruitments';
        };
        xhr.send(strngf);
    },

    handleDeleteElement(index, event) {
        var arr = this.state.elementsList;
        arr.splice(index, 1);

        this.setState({
            elementsList: arr,
        });
    },

    render() {
        var today = new Date().toISOString().split('T')[0];
        const htmlList = this.state.elementsList.map((element, index) => <Element key={index} dataObject={element} onDelete={this.handleDeleteElement.bind(null,index)}></Element>);
        return (
            <div id="formCreatorBody">
                <div id="recruitmentForm">
                    <form>
                        <div className="group">
                            <input id="title" name="formTitle" type="text" onChange={(event) => this.handleText(event)} required="required" />
                            <span className="highlight"></span><span className="bar"></span>
                            <label className="textLabel">Tytuł rekrutacji</label>
                        </div>
                        <div className="group">
                            <textarea id="recruitmentDesc" name="formDesc" onChange={(event) => this.handleText(event)} required ></textarea>
                            <span className="highlight"></span><span className="bar"></span>
                            <label className="textLabel">Opis stanowiska</label>
                        </div>
                        <div className="group">
                            <textarea id="recruitmentDemands" name="formDemand" onChange={(event) => this.handleText(event)} required ></textarea>
                            <span className="highlight"></span><span className="bar"></span>
                            <label className="textLabel">Wymagania na stanowisko</label>
                        </div>
                        <div className="group">
                            <textarea id="recruitmentOffer" name="formOffer" onChange={(event) => this.handleText(event)} required ></textarea>
                            <span className="highlight"></span><span className="bar"></span>
                            <label className="textLabel">Oferta</label>
                        </div>
                        <div className="group">
                            <input id="recruitmentPrice" name="formPrice" type="text" onChange={(event) => this.handleText(event)} required />
                            <span className="highlight"></span><span className="bar"></span>
                            <label className="textLabel">Oferowana płaca</label>
                        </div>
                        <div className="group">
                            <label className="">Data zakończenia rekrutacji</label>
                            <input name="formDate" type="date" min={today} onChange={(event) => this.handleText(event)} required />
                        </div>
                    </form>
                </div>

                <div id="formElementsList">
                    <p>Lista dodanych elementow</p>
                    <br />
                    <ul>
                        {htmlList}
                    </ul>
                </div>
                <div className="btn-box">
                    <AddButton onClick={() => this.handleModal()} />
                    <button className="btn btn-submit" type="button" onClick={() => this.hanldeFormCollectionSubmit()}>Zatwierdz</button>
                </div>
                <Modal show={this.state.isModalOpen} onClose={() => this.handleModal()} onSubmit={(event) => this.handleModalSubmit(event)} optionsList={this.state.inputOptions}>
                    Heres something for ya
                </Modal>
            </div>
        );
    }
});

var AddButton = React.createClass({
    render() {
        return (<button className="btn btn-cancel" onClick={() => this.props.onClick()}>Dodaj element</button>);
    }
});


ReactDOM.render(
    <FormCollection submitUrl="/recruitments/new"/>,
    document.getElementById('root')
);