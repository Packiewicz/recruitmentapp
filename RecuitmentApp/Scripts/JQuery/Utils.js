﻿function filterRecruitments(element) {
    var value = $(element).val();
    value = value.toString().toLowerCase();

    $("#recruitmentsList > div .panel-heading").each(function () {
        var tempText = $(this).text().toString().toLowerCase();

        if (value == '' || tempText.indexOf(value) > -1) {
            $(this).parent().show();
        }
        else {
            $(this).parent().hide();
        }
    });
}

function filterAppliactions(element) {
    var value = $(element).val();
    value = value.toString().toLowerCase();

    $("#applicationsList > div .panel-heading").each(function () {
        var tempText = $(this).text().toString().toLowerCase();

        if (value == '' || tempText.indexOf(value) > -1) {
            $(this).parent().show();
        }
        else {
            $(this).parent().hide();
        }
    });
}

function filterAdminRecruitments(element) {
    var value = $(element).val();
    value = value.toString().toLowerCase();

    $('div[id="recruitmentTitle"]').each(function () { //zwykly selektor id, wybiera tylko pierwszy z wielu
        var tempText = $(this).text().toString().toLowerCase();

        if (value == '' || tempText.indexOf(value) > -1) {
            $(this).parent().show();
        }
        else {
            $(this).parent().hide();
        }
    });
}

function filterRecruitmentActive(element) {
    var value = $(element).val();
    value = value.toString().toLowerCase();
    console.log(value);

    $('div[id="recruitmentElement"]').each(function () {
        var atr = $(this).attr("data-active");

        if (value == 'active')
        {
            if (atr == 'true')  // show active
            {
                $(this).show();
            }
            else
            {
                $(this).hide();
            }
        }
        else if (value == 'inactive')
        {
            if (atr == 'true')          // show inactive
            {
                $(this).hide();
            }
            else {
                $(this).show();
            }
        }
        else
        {
            $(this).show();     // always show
        }
    });
    
}